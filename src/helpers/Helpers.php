<?php

namespace Src\helpers;

class Helpers {
	function putJson($users, $entity) {
		file_put_contents(dirname(__DIR__, 2) . '/scripts/'.$entity.'.json', json_encode($users, JSON_PRETTY_PRINT));
	}
  function validatePhone(string $phone): bool {
    $baseURL = 'http://apilayer.net/api/validate'; // ?access_key=181ef2aa3b268f25826dd1b18a3819a2&number=14158586273&format=1'
    $key = getenv('NUM_VERIFY_KEY');

    $params = http_build_query([
      'access_key'  => $key,
      'number'      => $phone
    ]);
    
    $url = "$baseURL?$params";
    if($curl = curl_init()){
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_URL, $url);

      $result = curl_exec($curl);
      curl_close($curl);
      
      if($result = json_decode($result)) {
        return $result->valid;
      }
    }

    return false;
  }
	public static function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}
}