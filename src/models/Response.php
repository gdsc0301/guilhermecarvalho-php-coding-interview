<?php
namespace Src\models;

class Response {
  public int $status;
  public string $message;
  public mixed $data;

  function __construct(string $message = '', int $status = 200, $data = []) {
    $this->message = $message;
    $this->status = $status;
    $this->data = $data;
  }
}