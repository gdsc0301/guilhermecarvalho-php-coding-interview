<?php

namespace Src\models;
use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
  private ClientModel $clientModel;
  private DogModel $dogModel;
  private $helper;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
    
    $this->helper = new Helpers();
    $this->clientModel = new ClientModel();
    $this->dogModel = new DogModel();
	}

	public function getBookings() {
		return $this->bookingData;
	}

  public function createBooking($bookingData) {
    $bookings = $this->getBookings();

    $clientDogs = $this->dogModel->getClientDogs(intval($bookingData['clientid']));
    $clientDogsAges = array_map(
      fn($dog) => $dog['age'],
      $clientDogs
    );
    
    $averageClientDogAge = array_reduce(
      $clientDogsAges,
      fn($pv, $age) => $pv + $age,
      0
    ) / count($clientDogsAges);
    
    if($averageClientDogAge < 10)
      $bookingData['price'] = floatval($bookingData['price']) * .9;

		$bookingData['id'] = end($bookings)['id'] + 1;
		$bookings[] = $bookingData;

		$this->helper->putJson($bookings, 'bookings');

		return $bookingData;
  }
}