<?php

namespace Src\models;

use Src\helpers\Helpers;
use Src\models\Response;

class ClientModel {

	private $clientData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients(): Response {
		return new Response(data: $this->clientData);
	}

	public function createClient(array $data): Response {
    if(!$this->checkClientPhone($data))
      return new Response('Invalid phone', 400);
    
    if(!$this->checkClientEmail($data))
      return new Response('Invalid email', 400);

    $clients = $this->clientData;

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		$this->helper->putJson($clients, 'clients');

		return new Response(message: 'Client created successfully', data: $data);
	}

	public function updateClient(array $data): Response {
    if(!isset($data['id']) || empty($data['id']))
      return new Response('Invalid client', 400);

    if(isset($data['phone']) && !$this->checkClientPhone($data))
      return new Response('Invalid phone', 400);

    if(isset($data['email']) && !$this->checkClientEmail($data))
      return new Response('Invalid email', 400);

		$updateClient = [];
		$clients = $this->clientData;
		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

		$this->helper->putJson($clients, 'clients');

		return new Response(message: 'Client updated successfully', data: $updateClient);
	}

	public function getClientById(int $id): Response {
		$clients = $this->clientData;
    
		foreach ($clients as $client) {
			if ($client['id'] === $id) {
				return new Response(data: $client);
			}
		}

		return new Response(message: 'No client found with the ID provided');
	}

  private function checkClientPhone(array $clientData): bool {
    if(isset($clientData['phone']) && !empty($clientData['phone']))
      return $this->helper->validatePhone($clientData['phone']) === true;

    return false;
  }

  private function checkClientEmail(array $clientData): bool {
    if(isset($clientData['email']) && !empty($clientData['email']))
      return filter_var($clientData['email'], FILTER_VALIDATE_EMAIL) === $clientData['email'];

    return false;
  }
}