<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel {

	private $dogData;

	function __construct() {
		$this->helper = new Helpers();
    
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/dogs.json');
		$this->dogData = json_decode($string, true);
	}

	public function getDogs() {
		return $this->dogData;
	}

  public function getClientDogs(int $clientid) {
    return array_filter($this->getDogs(), fn($dog) => intval($dog['clientid']) === $clientid);
  }
}