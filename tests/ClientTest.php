<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Client;

class ClientTest extends TestCase {

	private $client;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->client = new Client();
	}

	/** @test */
	public function getClients() {
		$results = $this->client->getClients()->data;

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['username'], 'arojas');
		$this->assertEquals($results[0]['name'], 'Antonio Rojas');
		$this->assertEquals($results[0]['email'], 'arojas@dogeplace.com');
		$this->assertEquals($results[0]['phone'], '1234567');
	}

  /** @test */
	public function createClient() {
		$mockClient = [
			'username'  => 'newuser',
			'name'      => 'New User',
			'email'     => 'newuser+dogeplace.com',
			'phone'     => '5555555'
		];

		$response = $this->client->createClient($mockClient);
    $this->assertEquals(400, $response->status, 'Expects to return error in the phone number');

    $mockClient['phone'] = '5511977615907';
		$response = $this->client->createClient($mockClient);
    $this->assertEquals(400, $response->status, 'Expects to return error in the email');

    $mockClient['email'] = 'newuser@dogeplace.com';
		$response = $this->client->createClient($mockClient);
    $this->assertEquals(200, $response->status, 'Expects to create a new Client successfully');

		$results = $this->client->getClients()->data;
    $this->assertEquals($response->data, end($results), 'Expects the latest created Client to have the same attributes as we sent');
		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}

  /** @test */
	public function updateClient() {
		$mockClient = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'cperez@dogeplace.com',
			'phone' => '2222222'
		];

		$updateRes = $this->client->updateClient($mockClient);
    $this->assertEquals(400, $updateRes->status);

    $mockClient['phone'] = '5511977615907';
		$updateRes = $this->client->updateClient($mockClient);
    $this->assertEquals(200, $updateRes->status);

		$results = $this->client->getClients()->data;
    $this->assertEquals(end($results), $mockClient);
		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}
}