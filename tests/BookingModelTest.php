<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\models\BookingModel;

class BookingModelTest extends TestCase {
	private $bookingModel;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->bookingModel = new BookingModel();
	}

  /** @test */
  public function createBooking() {
    $newBooking = [
      'clientid'    => 2,
      'price'       => 300,
      'checkindate' => '2022-08-04 15:00:00',
      'checkoutdate'=> '2021-08-11 15:00:00'
    ];

    $result = $this->bookingModel->createBooking($newBooking);
    $this->assertIsArray($result);

    $this->assertEquals($result['clientid'], $newBooking['clientid']);
    $this->assertEquals(
      $result['price'],
      floatval($newBooking['price']) * .9,
      'Price got discount: ' . $result['price']
    );
  }
}